# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table admin (
  id                            integer auto_increment not null,
  user_id                       integer,
  is_super_admin                tinyint(1) default 0,
  constraint pk_admin primary key (id)
);

create table bed (
  id                            integer auto_increment not null,
  room_id                       integer,
  username                      varchar(255),
  password                      varchar(255),
  active_flag                   tinyint(1) default 0,
  bind_token                    varchar(255),
  constraint pk_bed primary key (id)
);

create table department (
  id                            integer auto_increment not null,
  name                          varchar(255),
  active_flag                   tinyint(1) default 0,
  constraint pk_department primary key (id)
);

create table linked_account (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  provider_user_id              varchar(255),
  provider_key                  varchar(255),
  constraint pk_linked_account primary key (id)
);

create table patient (
  id                            integer auto_increment not null,
  fname                         varchar(255),
  lname                         varchar(255),
  bed_id                        integer,
  diagnostic                    varchar(255),
  age                           integer,
  admission_date                datetime(6),
  extern_date                   datetime(6),
  active_flag                   tinyint(1) default 0,
  constraint uq_patient_bed_id unique (bed_id),
  constraint pk_patient primary key (id)
);

create table person (
  id                            integer auto_increment not null,
  name                          varchar(255),
  constraint pk_person primary key (id)
);

create table role (
  id                            integer auto_increment not null,
  name                          varchar(255),
  color                         varchar(255),
  constraint pk_role primary key (id)
);

create table room (
  id                            integer auto_increment not null,
  title                         varchar(255),
  department_id                 integer,
  active_flag                   tinyint(1) default 0,
  constraint pk_room primary key (id)
);

create table security_role (
  id                            bigint auto_increment not null,
  role_name                     varchar(255),
  constraint pk_security_role primary key (id)
);

create table staff (
  id                            integer auto_increment not null,
  fname                         varchar(255),
  lname                         varchar(255),
  constraint pk_staff primary key (id)
);

create table token_action (
  id                            bigint auto_increment not null,
  token                         varchar(255),
  target_user_id                bigint,
  type                          varchar(2),
  created                       datetime(6),
  expires                       datetime(6),
  constraint ck_token_action_type check (type in ('PR','EV')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id)
);

create table users (
  id                            bigint auto_increment not null,
  email                         varchar(255),
  name                          varchar(255),
  first_name                    varchar(255),
  last_name                     varchar(255),
  last_login                    datetime(6),
  active                        tinyint(1) default 0,
  email_validated               tinyint(1) default 0,
  constraint pk_users primary key (id)
);

create table users_security_role (
  users_id                      bigint not null,
  security_role_id              bigint not null,
  constraint pk_users_security_role primary key (users_id,security_role_id)
);

create table users_user_permission (
  users_id                      bigint not null,
  user_permission_id            bigint not null,
  constraint pk_users_user_permission primary key (users_id,user_permission_id)
);

create table user_permission (
  id                            bigint auto_increment not null,
  value                         varchar(255),
  constraint pk_user_permission primary key (id)
);

alter table bed add constraint fk_bed_room_id foreign key (room_id) references room (id) on delete restrict on update restrict;
create index ix_bed_room_id on bed (room_id);

alter table linked_account add constraint fk_linked_account_user_id foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_linked_account_user_id on linked_account (user_id);

alter table patient add constraint fk_patient_bed_id foreign key (bed_id) references bed (id) on delete restrict on update restrict;

alter table room add constraint fk_room_department_id foreign key (department_id) references department (id) on delete restrict on update restrict;
create index ix_room_department_id on room (department_id);

alter table token_action add constraint fk_token_action_target_user_id foreign key (target_user_id) references users (id) on delete restrict on update restrict;
create index ix_token_action_target_user_id on token_action (target_user_id);

alter table users_security_role add constraint fk_users_security_role_users foreign key (users_id) references users (id) on delete restrict on update restrict;
create index ix_users_security_role_users on users_security_role (users_id);

alter table users_security_role add constraint fk_users_security_role_security_role foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;
create index ix_users_security_role_security_role on users_security_role (security_role_id);

alter table users_user_permission add constraint fk_users_user_permission_users foreign key (users_id) references users (id) on delete restrict on update restrict;
create index ix_users_user_permission_users on users_user_permission (users_id);

alter table users_user_permission add constraint fk_users_user_permission_user_permission foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;
create index ix_users_user_permission_user_permission on users_user_permission (user_permission_id);


# --- !Downs

alter table bed drop foreign key fk_bed_room_id;
drop index ix_bed_room_id on bed;

alter table linked_account drop foreign key fk_linked_account_user_id;
drop index ix_linked_account_user_id on linked_account;

alter table patient drop foreign key fk_patient_bed_id;

alter table room drop foreign key fk_room_department_id;
drop index ix_room_department_id on room;

alter table token_action drop foreign key fk_token_action_target_user_id;
drop index ix_token_action_target_user_id on token_action;

alter table users_security_role drop foreign key fk_users_security_role_users;
drop index ix_users_security_role_users on users_security_role;

alter table users_security_role drop foreign key fk_users_security_role_security_role;
drop index ix_users_security_role_security_role on users_security_role;

alter table users_user_permission drop foreign key fk_users_user_permission_users;
drop index ix_users_user_permission_users on users_user_permission;

alter table users_user_permission drop foreign key fk_users_user_permission_user_permission;
drop index ix_users_user_permission_user_permission on users_user_permission;

drop table if exists admin;

drop table if exists bed;

drop table if exists department;

drop table if exists linked_account;

drop table if exists patient;

drop table if exists person;

drop table if exists role;

drop table if exists room;

drop table if exists security_role;

drop table if exists staff;

drop table if exists token_action;

drop table if exists users;

drop table if exists users_security_role;

drop table if exists users_user_permission;

drop table if exists user_permission;


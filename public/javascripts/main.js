$(document).ready(function () {
    $("li[id^='person_'] span").click(function(){
        person = $(this).parent();
        personName = person.find("a").text();
        person_id = person.attr("id");
        person_id = person_id.split("_");
        if($.isArray(person_id)){
            person_id = person_id[1];
        }
        else{
            return false;
        }

        var question = confirm("Are you sure you want to remove " + personName);
        if (question == true) {
            $.ajax({
                method: "POST",
                url: "/remove-person/" + person_id,
                data: {}
            }).done(function( msg ) {
                if(msg == 1){
                    person.remove();
                }
                else{
                    alert("Could not remove person with ID " + person_id);
                }
            });
        }
    })
});
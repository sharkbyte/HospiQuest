$(function() {
    var active_department;
    var active_room;
    var active_bed;
    var active_patient;
    var department_selected;
    var room_selected;
    var bed_selected;

    $('table').footable();
//Overlay JavaScripts
    // Display second table on first table click
    $(document).on("click", ".department-table .department td:first-child", function(){
        $(".room-list").hide();
        $(".bed-list").hide();
        var department_id = $(this).parent().attr("id").split("_")[1];
        $(".room").hide();
        $(".room-table .department_" + department_id).show();
        $(".room-list").show(600);
    });

    //See active left menu
    $(".sidebar-wrapper .nav li").click(function () {
        $(".sidebar-wrapper .nav li").find(".active").removeClass('active');
        $(this).addClass("active");
    });

    // See active row on click
    $(".department-table-list table tbody tr").click(function () {
        $(".department-table-list table tbody tr").removeClass('active-department');
        /* Remove active class for room & bed when other department is selected */
        $(".room-table-list table tbody tr").removeClass('active-department');
        $(".bed-table-list table tbody tr").removeClass('active-department');
        $(this).addClass("active-department");
    });
    $(".room-table-list table tbody tr").click(function () {
        $(".room-table-list table tbody tr").removeClass('active-department');
        $(".bed-table-list table tbody tr").removeClass('active-department');
        $(this).addClass("active-department");

    });
    $(".bed-table-list table tbody tr").click(function () {
        $(".bed-table-list table tbody tr").removeClass('active-department');
        $(this).addClass("active-department");
    });

    // Display last table on second table click
    $(document).on("click", ".room-table tbody tr", function(){
        $(".bed-list").hide();
        $(".bed-table").hide();
        $(".no-bed").hide();
        $(".bed").hide();
        var room_id = $(this).attr("id").split("_")[1];
        $(".bed-table .room_" + room_id).show();
        if ($(".room_" + room_id).length > 0 ){
            $(".room_" + room_id).show();
            $(".bed-table").show();
        }
        else {
            $(".no-bed").show(600);
        }
        $(".bed-list").show(600);
    });

//End Overlay JavaScripts

//Department JS
    // Department find active department
    $(".department-table tbody>tr").click(function(){
        var dep_anchor = $(this).attr("id");
        var dep_name = $(this).find(".department-column").text();
        var dep_id = dep_anchor.split("_");
        active_department = dep_id[1];
        if(active_department > 0){
            $("#dep-name").attr("dep-id", active_department).val(dep_name);
        }
        $("#department_delete_name").text(dep_name);
        $("#department_edit_name").text(dep_name);
        $("#dep_id_add_rooms").val(active_department);
    });

    // Delete department function
    $("#delete-dep").click(function() {
        if (active_department > 0) {
            $.ajax({
                method: "POST",
                url: "/admin/delete-department/" + active_department,
                data: {}
            }).done(function( msg ) {
                if(msg == 1){
                    $("#department_" + active_department).remove();
                    $('#delete-department').modal('hide');
                }
                else{
                    alert("Could not remove department with ID " + active_department);
                }
            });
        }
        else {
            alert("No department selected");
        }
    });

    //Edit department function
    $("#edit-dep-modal").click(function() {
        var department_id = $("#dep-name").attr("dep-id");
        var department_name = $("#dep-name").val();
        if (department_id > 0) {
            $.ajax({
                method: "POST",
                url: "/admin/edit-department/" + department_id,
                data: {department_name:department_name}
            }).done(function( msg ) {
                if(msg > 0){
                    $("#department_" + department_id + " .department-column").text(department_name);
                    $('#edit-department').modal('hide');
                }
                else{
                    alert("Could not edit department with ID " + department_id);
                }
            });
        }
        else {
            alert("No department selected");
        }
    });

//End Department JS



//Room JS
    //Get active Room
    $(".room-table tbody>tr").click(function(){
        var room = $(this);
        var room_anchor = room.attr("id");
        var room_name = room.find(".room-column").text();
        var room_id = room_anchor.split("_");
        active_room = room_id[1];
        if(active_room > 0){
            $("#room-name").attr("room-id", active_room).val(room_name);
        }
        $("#room_delete_name").text(room_name);
        $("#room_edit_name").text(room_name);
        $("#room_id_add_beds").val(active_room);
    });

    //Edit Room function
    $("#edit-room-modal").click(function() {
        var room_id = $("#room-name").attr("room-id");
        var room_name = $("#room-name").val();
        if (room_id > 0) {
            $.ajax({
                method: "POST",
                url: "/admin/edit-room/" + room_id,
                data: {room_name:room_name}
            }).done(function( msg ) {
                if(msg > 0){
                    $("#room_" + room_id + " .room-column").text(room_name);
                    $('#edit-room').modal('hide');
                }
                else{
                    alert("Could not edit room with ID " + room_id);
                }
            });
        }
        else {
            alert("No room selected");
        }
    });

    // Delete active room ajax
    $("#del-room").click(function() {
        if (active_room > 0) {
            $.ajax({
                method: "POST",
                url: "/admin/delete-room/" + active_room,
                data: {}
            }).done(function( msg ) {
                if(msg == 1){
                    $("#room_" + active_room).remove();
                    $('#delete-room').modal('hide');
                }
                else{
                    alert("Could not remove room with ID " + active_room);
                }
            });
        }
        else {
            alert("No room selected");
        }
    });
//End Room JS



//Bed JS
    // Add new bed form settings
    $("#add-bed-button").click(function(e){
        e.preventDefault();
        var username = $("#add-beds input[name='username']").val();
        var password = $("#add-beds input[name='password']").val();
        var room_id = $("#add-beds input[name='room_id']").val();

        if(username.length <= 3 || password.length <= 3){
            alert("Username and password must have at least 3 characters");
            return;
        }

        if(room_id > 0){
            $.ajax({
                method: "POST",
                url: "/admin/add-beds",
                data: {
                    'username': username,
                    'password': password,
                    'room_id':  room_id
                }
            }).done(function( bed_id ) {
                if(bed_id > 0){
                    $(".bed-list tbody").prepend(
                        '<tr id="bed_' + bed_id + '" class="bed room_' + active_room + '" style="display: table-row;">' +
                            '<td class="bed-column" style="display: table-cell;">' +
                                'Bed #' + bed_id +
                            '</td>' +
                            '<td class="table-no-patient" style="display: table-cell;">' +
                                'No Patient' +
                            '</td>' +
                            '<td style="display: table-cell;">' +
                                '<i class="fa fa-plus-square-o add-patient" aria-hidden="true" data-toggle="modal" data-target="#add-patient-in-bed"></i>' +
                                '<i class="fa fa-pencil-square-o" aria-hidden="true" data-toggle="modal" data-target="#edit-bed"></i>' +
                                '<i class="fa fa-times delete-bed" aria-hidden="true" data-toggle="modal" data-target="#delete-bed"></i>' +
                             '</td>' +
                        '</tr>');
                    $("#add-beds").modal("hide");
                }
                else{
                    alert("username exists");
                }

            });
        }
        else{
            alert("room id not set");
        }
    });

    //Edit bed function
     $("#edit-bed-modal").click(function() {
         var bed_username = $("#bed-username").val();
         var bed_password = $("#bed-password").val();
         if (active_bed > 0) {
             $.ajax({
                 method: "POST",
                 url: "/admin/edit-bed/" + active_bed,
                 data: {bed_username:bed_username, bed_password:bed_password}
             }).done(function( msg ) {
                 if(msg > 0){
                     $('#edit-bed').modal('hide');
                 }
                 else{
                     alert("Could not edit bed with ID " + bed_id);
                 }
             });
         }
         else {
             alert("No bed selected");
         }
     });


    // Delete active bed function
    $(".bed-table tbody>tr").click(function(){
        $(".bed-details").show(200);
        var bed = $(this);
        var bed_anchor = bed.attr("id");
        var bed_id = bed_anchor.split("_");
        if (bed.find("td:first").length>0){
            var bed_name = bed.find("td:first").text();
        }
        active_bed = bed_id[1];
        if(active_department > 0) {
            $("#bed-name").attr("bed-id", active_bed).val(bed_name);
        }
        $("#bed_delete_name").text(bed_name);
        $("#bed_edit_name").text(bed_name);
    });

    // Delete active bed ajax function
    $("#del-bed").click(function() {
        alert (active_bed);
        if (active_bed > 0) {
            $.ajax({
                method: "POST",
                url: "/admin/delete-bed/" + active_bed,
                data: {}
            }).done(function( msg ) {
                if(msg == 1){
                    $("#bed_" + active_bed).remove();
                    $('#delete-bed').modal('hide');
                }
                else{
                    alert("Could not remove bed with ID " + active_bed);
                }
            });
        }
        else {
            alert("No bed selected");
        }
    });

    // Add Bed #ID for new patient in bed
    $(".bed-table-list tbody>tr").click(function () {
        $("#bed_id").val(active_bed);
    });

    $(".bed-table-list .patient-details").click(function () {
        var bed_row = $(this).closest("tr");
        var patient = bed_row.attr("patient-id");
        var selected_patient = patient.split("_")[1];
        active_patient = selected_patient;
        $.ajax({
            method: "POST",
            url: "/admin/get-patient/" + selected_patient,
            data: {}
        }).done(function( patient ) {
            patient = JSON.parse(patient);
            if(patient == 0){
                alert("Could not show details for patient with ID " + selected_patient);
            }
            else{
                $("#patient-details .patient-name").text(patient.first_name + ' ' + patient.last_name);
                $("#patient-details .patient-age").text(patient.age);
                $("#patient-details .patient-diagnostic").text(patient.diagnostic);
                $("#patient-details .patient-admission").text(formatDate(patient.admission));
                $("#patient-details .patient-extern").text(formatDate(patient.admission));
                $("#patient-details .patient-department").text(patient.department);
                $("#patient-details .patient-room").text(patient.room);
                $("#patient-details .patient-bed").text(patient.bed);
            }
        });
    });

//End Bed JS



//Patient JS

    // Check if department/room/bed are selected before add patient in bed
    $('#add-patient').submit(function(){
        var department_selected = $("#add-patient .drop-department button").attr("value");
        var room_selected = $("#add-patient .drop-room button").attr("value");
        var bed_selected = $("#add-patient .drop-bed button").attr("value");

        if(department_selected === undefined){
            sweetAlert({
                title: "Department not selected!",
                text: "Please select a department",
                type: "error"
            });
            return false;
        }
        else{
            if(room_selected === undefined){
                sweetAlert({
                    title: "Room not selected!",
                    text: "Please select a room from your department",
                    type: "error"
                });
                return false;
            }
            else{
                if(bed_selected === undefined){
                    sweetAlert({
                        title: "Bed not selected!",
                        text: "Assign a bed for this patient",
                        type: "error"
                    });
                    return false;
                }
                else{
                    return true;
                }
                return true;
            }
            return true;
        }

    });

    $("#add-patient .drop-room button").on('click', function () {
        if(department_selected === undefined){
            $(".drop-room ul li").hide();
            $(".drop-room .dropdown-menu .department-empty").hide();
            $(".drop-room ul .room-error").toggle();
            $(".drop-room ul li").show(300);
            $(".drop-room .dropdown-menu .department-empty").hide();
        }

        var room_text = $('#add-patient .drop-room ul li').text();
        var room_length = room_text.length;
        if(room_length == 0 ) {
            $(".drop-room ul li").hide();
            $(".drop-room ul").append(
                '<li class="department-empty empty-li">' +
                '<i class="fa fa-info-circle" aria-hidden="true"></i>' +
                'Department empty' +
                '</li>'
            );
        }
    });

    $("#add-patient .drop-bed button").on('click', function () {
        if(room_selected === undefined){
            $(".drop-bed ul li").hide();
            $(".drop-bed ul .bed-error").toggle();
            $(".drop-bed ul li").show(300);
        }

       var bed_text = $("#add-patient .drop-bed ul li").text();
       var bed_length = bed_text.length;
       if(bed_length == 0 ) {
           $(".drop-bed ul li").hide();
           $(".drop-bed ul").append(
               '<li class="room-empty empty-li">' +
               '<i class="fa fa-info-circle" aria-hidden="true"></i>' +
               'Room empty' +
           '</li>'
           );
       }
    });

    // Delete active patient
    $(".patients-table tbody>tr ").click(function(){
        var patient_extern = $(this).find(".patient-name").text()
        $("#patient_extern_name").text(patient_extern);
        try {
            var patient = $(this);
            var patient_anchor = patient.attr("id");
            var patient_id = patient_anchor.split("_");
            active_patient = patient_id[1];

            $.ajax({
                method: "POST",
                url: "/admin/get-patient/" + active_patient,
                data: {}
            }).done(function( patient ) {
                patient = JSON.parse(patient);
                if(patient == 0){
                    alert("Could not show details for patient with ID " + active_patient);
                }
                else{
                    $("#patient-name").text(patient.first_name + ' ' + patient.last_name);
                    $("#patient-age").text(patient.age);
                    $("#patient-diagnostic").text(patient.diagnostic);
                    $("#patient-admission").text(formatDate(patient.admission));
                    $("#patient-extern").text(formatDate(patient.extern));
                    $("#patient-department").text(patient.department);
                    $("#patient-room").text(patient.room);
                    $("#patient-bed").text(patient.bed);
                }
            });

        }catch(e) {
            alert ("Could not find patient data");
            throw e.message;
        }
    });

    $("#extern-patient").click(function() {
        if (active_patient > 0) {
            $.ajax({
                method: "POST",
                url: "/admin/remove-patient/" + active_patient,
                data: {}
            }).done(function( msg ) {
                if(msg == 1){
                    $("#patient_" + active_patient).remove();
                    $('#extern').modal('hide');
                }
                else{
                    alert("Could not remove patient with ID " + active_patient);
                }
            });
        }
        else {
            alert("No patient selected");
        }
    });





//End Patient JS



// Start -- Get Departments & Rooms in AddPatient Form
    $(".drop-department li a").click(function(){
        var department_id = $(this).parent().attr("department-id");
        $.ajax({
            method: "POST",
            url: "/admin/get-rooms/by-department/" + department_id,
            data: {}
        }).done(function( msg ) {
            var rooms = JSON.parse(msg);
            $(".drop-room .dropdown-menu").html('');
            $.each(rooms, function(k,v){
                $(".drop-room .dropdown-menu").append(
                    '<li room-id="'+ k +'" class="room-option">' +
                        '<a href="#">' + v + '</a>'+
                    '</li>');
            });
        });
    });

    $(document).on("click", ".drop-room li a", function(){
        var room_id = $(this).parent().attr("room-id");
        $.ajax({
            method: "POST",
            url: "/admin/get-beds/by-room/" + room_id,
            data: {}
        }).done(function( message ) {
            var beds = (JSON).parse(message);
            $(".drop-bed .dropdown-menu").html('');
            $.each(beds, function (k,v) {
                $(".drop-bed .dropdown-menu").append(
                    '<li bed-id="'+ k +'" class="bed-option">' +
                        '<a href="#">' + v + '</a>'+
                    '</li>');
            });
        });
    });


    $(document).on("click", ".drop-bed li.bed-option", function(){
        var bed_id = $(this).attr("bed-id");
        $("#bed_id").val(bed_id);
    });

    $(".dropdown-menu").click(function (e) {
        if($(this).find("li").length <= 0){
            e.stopPropagation();
        }
    });

    $(document).on("click", ".dropdown-menu li a", function(){
        $(this).closest(".dropdown").find(".btn:first-child").html($(this).text() + ' <span class="caret"></span>');
        $(this).closest(".dropdown").find(".btn:first-child").val($(this).text());
    });
// End -- Get Departments & Rooms in AddPatient Form

    $('#admission-date').datetimepicker({
        viewMode: 'years'
    });

    $('#extern-date').datetimepicker({
        viewMode: 'years'
    });

    $("#add-patient .dates #admission-date").click(function () {
        $(".datetimepicker-dropdown-bottom-right:eq(0)").addClass("datepicker-admission");
    });

    $("#add-patient-in-bed .dates #admission-date").click(function () {
        $(".datetimepicker-dropdown-bottom-right:eq(0)").addClass("datepicker-bed-admission");
    });

    $("#add-patient .dates #extern-date").click(function () {
        $(".datetimepicker-dropdown-bottom-right:eq(1)").addClass("datepicker-extern");
    });

    $("#add-patient-in-bed .dates #extern-date").click(function () {
        $(".datetimepicker-dropdown-bottom-right:eq(1)").addClass("datepicker-bed-extern");
    });
});

function formatDate(timestamp){
    var date = new Date(parseInt(timestamp));
    if(date != null && parseInt(timestamp) > 0 ){
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return day + '-' + monthIndex + '-' + year;
    }
    return "Not Available";

}


function html_parser(element) {
    return element.context.innerHTML;
}
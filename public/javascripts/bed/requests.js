/**
 * Created by sandu on 10/7/2016.
 */
var bed = {
    getBindToken: function(){
        var cookies = Cookies.get();
        if(cookies.hasOwnProperty("bedToken")){
            return cookies.bedToken;
        }
        return null;
    },
    setBindTokenCookie: function(token){
       Cookies.set('bedToken', token, { expires: 10000 });
    }
};

$(document).ready(function(){

});
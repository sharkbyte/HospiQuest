package controllers;

import models.Bed;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.UserProvider;
import views.html.bed.*;

import javax.inject.Inject;

import static play.libs.Json.toJson;


/**
 * Created by Marius Sandu on 8/15/2016.
 */
public class Beds extends Controller {

    private final UserProvider userProvider;
    private final FormFactory formFactory;

    @Inject
    public Beds(final UserProvider userProvider, final FormFactory formFactory){
        this.userProvider = userProvider;
        this.formFactory = formFactory;
    }

    public Result index() throws Exception{
        // checks if the user is logged in
        boolean isLoggedIn = Admin.loggedIn();
        if(!isLoggedIn){
            Http.Session session = ctx().session();
            if(session.containsKey("bedToken") || request().cookies().get("bedToken") != null){
                try{
                    String token = session.containsKey("bedToken") ? session.get("bedToken") : request().cookies().get("bedToken").value() ;
                    if(!session.containsKey("bedToken")){
                        session.put("bedToken", token);
                    }
                    Bed loggedInBed = Bed.getBedByToken(token);
                    return ok(views.html.bed.requests.render(loggedInBed.getBind_token(), this.userProvider));
                }catch(Exception e){
                    session.remove("bedToken");
                    session.remove("bedLoggedIn");
                    return ok(bed.render(this.userProvider));
                }
            }
            return ok(bed.render(this.userProvider));
        }else{
            return redirect(routes.Admin.index());
        }
    }

    public Result requestsAction() throws Exception {
        Bed bed = this.formFactory.form(models.Bed.class).bindFromRequest().get();
        bed.setPassword(Admin.encodeBedPassword(bed.getPassword()));
        Http.Session session = ctx().session();
        try{
            Bed loggedInBed = Bed.canLoginAsBed(bed);
            session.put("bedLoggedIn", "true");
            session.put("bedToken", loggedInBed.getBind_token());

            return ok(views.html.bed.requests.render(loggedInBed.getBind_token(), this.userProvider));
        }catch(Exception e){
            session.remove("bedToken");
            session.remove("bedLoggedIn");
            return ok(views.html.bed.bed.render(this.userProvider));
        }
    }

    public Result checkBedLoginByToken(String token){
        Http.Session session = ctx().session();
        try{
            Bed loggedInBed = Bed.getBedByToken(token);
            if(loggedInBed != null){
                session.put("bedLoggedIn", "true");
                session.put("bedToken", token);
                return ok(toJson("1"));
            }
            return ok(toJson("2"));
        }catch(Exception e){
            session.remove("bedToken");
            session.remove("bedLoggedIn");
            return ok(bed.render(this.userProvider));
        }
    }
}

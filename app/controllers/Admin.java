package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.google.gson.Gson;
import com.google.inject.Inject;
import models.*;
import play.Logger;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Html;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.admin.*;
import java.text.ParseException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

public class Admin extends Controller {

	public static final String USER_ROLE = "user";

	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;

	private Boolean isAdmin;

	private final Html assets;

	private final Html errorView;

	private final Html sidebar;

	private final Html header;

	private final Gson gson = new Gson();

	private final Integer MAX_ROOMS_IN_DEPARTMENT = 20;

	public static String formatTimestamp(final long t) {
		return new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(new Date(t));
	}

	@Inject
	FormFactory formFactory;


	@Inject
	public Admin(final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
                 final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
		this.assets = admin_assets.render();
		this.errorView = views.html.helper.errorView.render(403, "Forbidden");
		this.sidebar = views.html.admin.sidebar.render();
		this.header = views.html.admin.header.render();
	}

	@Restrict(@Group(Admin.USER_ROLE))
	public Result index() {
		this.lookupAdminStatus();
		Html dashboardView = dashboard.render(this.sidebar, this.header, this.userProvider);

		return ok(index.render(this.isAdmin, dashboardView, this.assets,  this.errorView, this.userProvider));
	}

	@Restrict(@Group(Admin.USER_ROLE))
	public Result departments() {
		this.lookupAdminStatus();
		List<Department> departments = Department.getAllDepartments();
        Html add_patient_modal = views.html.admin.add_patient_modal.render(departments);

		Html departmentsView = views.html.admin.departments.render(departments, this.sidebar, this.header, add_patient_modal, this.userProvider);

		return ok(index.render(this.isAdmin, departmentsView, this.assets, this.errorView, this.userProvider));
	}
	@Restrict(@Group(Admin.USER_ROLE))
	public Result patients() {
		this.lookupAdminStatus();
		List<Department> departmentsList = Department.getAllDepartments();
		List<Patient> patientsList = Patient.getAllPatients();
        Html add_patient_modal = views.html.admin.add_patient_modal.render(departmentsList);
        Html patientsView = patients.render(patientsList, this.sidebar, this.header, add_patient_modal, this.userProvider);

		return ok(index.render(this.isAdmin, patientsView, this.assets, this.errorView, this.userProvider));
	}

// Department
	public Result addDepartment(){
	try{
		Integer number_of_rooms;
		Department department = formFactory.form(Department.class).bindFromRequest().get();
		if(department.getName().isEmpty()){
			return redirect(controllers.routes.Admin.departments());
		}
		department.setActive_flag(true);
		department.save();
		try{
			number_of_rooms = Integer.valueOf(formFactory.form().bindFromRequest().get("room_no"));
		}
		catch(Exception e){
			number_of_rooms = 0;
			Logger.info("Number of rooms defaulted to 0");
		}

		if(number_of_rooms > MAX_ROOMS_IN_DEPARTMENT){
			number_of_rooms = MAX_ROOMS_IN_DEPARTMENT;
		}

		this.addRoomsForDepartment(number_of_rooms, department);

	}
	catch (Exception e){
		Logger.error(e.getMessage());
		throw e;
	}

	return redirect(controllers.routes.Admin.departments());
}

	public Result editDepartment(Long department_id)
			throws Exception {
		Department department = Department.find.byId(department_id);
		String dep_name = formFactory.form().bindFromRequest().get("department_name");

		if (dep_name != null) {
			try{
				department.setName(dep_name);
				department.save();
				return ok("1");
			}
			catch(Exception e){
				throw e;
			}

		}
		else {
			throw new Exception("Can't update department name");
		}
	}

	public Result removeDepartment(Long department_id){
		this.lookupAdminStatus();
		if(this.isAdmin){
			return ok(this.gson.toJson(Department.removeDepartment(department_id)));
		}
		else{
			return ok(views.html.helper.errorPage.render(403, "Not allowed"));
		}
	}

// End department




// Room
	public Result getAllRoomsByDepartmentId(Long department_id){
		List<Room> rooms =  Room.getAllRoomsByDepartmentId(department_id);
		Map<Integer, String> roomsToReturn = new HashMap<>();
		for(Room room: rooms){
			roomsToReturn.put(room.getId(), room.getTitle());
		}
		return ok(gson.toJson(roomsToReturn));
	}

	public Result addRoomsToDepartment(){
	Integer number_of_rooms = Integer.valueOf(formFactory.form().bindFromRequest().get("room_no"));
	Long department_id = Long.valueOf(formFactory.form().bindFromRequest().get("dep_id"));
	Department department = Department.find.byId(department_id);
	this.addRoomsForDepartment(number_of_rooms, department);

	return redirect(controllers.routes.Admin.departments());
}

	private void addRoomsForDepartment(Integer number_of_rooms, Department department){
		for (int i = 0; i < number_of_rooms; i++) {
			Integer count = i+1;
			Room room = new Room();
			room.setDepartment(department);
			room.setTitle("Room #" + count.toString());
			room.setActive_flag(true);
			room.save();
		}
	}

	public Result editRoom(Long room_id)
		throws Exception {
	Room room = Room.find.byId(room_id);
	String room_name = formFactory.form().bindFromRequest().get("room_name");

	if (room_name != null) {
		try{
			room.setTitle(room_name);
			room.save();
			return ok("1");
		}
		catch(Exception e){
			throw e;
		}

	}
	else {
		throw new Exception("Can't update room name");
	}
}

	public Result removeRoom(Long room_id){
		this.lookupAdminStatus();
		if(this.isAdmin){
			return ok(this.gson.toJson(Room.removeRoom(room_id)));
		}
		else{
			return ok(views.html.helper.errorPage.render(403, "Not allowed"));
		}
	}

//End room




// Bed
	public Result getAllBedsByRoomId(Long room_id){
	List<Bed> beds =  Bed.getAllBedsByRoomId(room_id);
	Map<Integer, String> bedsToReturn = new HashMap<>();
	for (Bed bed: beds){
		if(bed.getPatient() == null ){
			bedsToReturn.put(bed.getId(), "Bed #" + bed.getId().toString());
		}
	}
	return ok(gson.toJson(bedsToReturn));
}

	private String generateBedToken() throws NoSuchAlgorithmException{
		String plaintext = UUID.randomUUID().toString();
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.reset();
		m.update(plaintext.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		while(hashtext.length() < 32 ){
			hashtext = "0"+hashtext;
		}
		return hashtext;
	}

	public static String encodeBedPassword(String password) throws NoSuchAlgorithmException{
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.reset();
		m.update(password.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		while(hashtext.length() < 32 ){
			hashtext = "0"+hashtext;
		}
		return hashtext;
	}

	public Result addBedsToRoom(){
	String username = formFactory.form().bindFromRequest().get("username");
	String password = formFactory.form().bindFromRequest().get("password");
	Long room_id = Long.valueOf(formFactory.form().bindFromRequest().get("room_id"));
	try{
		Room room = Room.find.byId(room_id);
		if(room != null){
			Integer bed_id = this.addBedForRoom(username, encodeBedPassword(password), room);
			if(bed_id != null && bed_id > 0 ){
				return ok(String.valueOf(bed_id));
			}
		}
		else{
			throw new Exception("no room found with id: " + room_id.toString());
		}
	}
	catch(Exception e){
		Logger.error(e.getMessage());
	}
	return ok("0");
}

	private Integer addBedForRoom(String username, String password, Room room)
			throws Exception
	{
		if(username.length() > 3 ||	password.length() > 3){
			Bed username_checker = Bed.find.where().eq("username", username).findUnique();
			if(username_checker != null){
				Logger.info("Username " + username + " already exists");
				return 0;
			}
			Bed bed = new Bed();
			bed.setRoom(room);
			bed.setUsername(username);
			bed.setPassword(password);
			bed.setActive_flag(true);
			bed.setBind_token(this.generateBedToken());
			bed.save();

            return bed.getId();
		}
		else {
			throw new Exception("bed username and password need to have more than 3 chars");
		}
	}

    public Result editBed(Long bed_id)
            throws Exception {
        Bed bed = Bed.find.byId(bed_id);
        String bed_username = formFactory.form().bindFromRequest().get("bed_username");
        String bed_password = formFactory.form().bindFromRequest().get("bed_password");

        if (bed_username != null) {
            try{
                bed.setUsername(bed_username);
                bed.setPassword(this.encodeBedPassword(bed_password));
                bed.save();
                return ok("1");
            }
            catch(Exception e){
                throw e;
            }

        }
        else {
            throw new Exception("Can't update bed name");
        }
    }

	public Result removeBed(Long bed_id) {
		this.lookupAdminStatus();
		if(this.isAdmin){
			return ok(this.gson.toJson(Bed.removeBed(bed_id)));
		}
		else{
			return ok(views.html.helper.errorPage.render(403, "Not allowed"));
		}
	}
//End Bed



// Patient
	public Result getPatientById (Long patient_id) {
		Patient patient = Patient.getPatientById(patient_id);
		Map<String, String> patientMap = new HashMap<>();
		if (patient != null) {
			patientMap.put("first_name", patient.getFname());
			patientMap.put("last_name", patient.getLname());
			patientMap.put("age", patient.getAge().toString());
			patientMap.put("diagnostic", patient.getDiagnostic());
			patientMap.put("admission", patient.getAdmission_date() != null ? String.valueOf(patient.getAdmission_date().getTime()) : "");
			patientMap.put("extern", patient.getExtern_date() != null ? String.valueOf(patient.getExtern_date().getTime()) : "");
			patientMap.put("department", patient.getBed() != null ? patient.getBed().getRoom().getDepartment().getName() : "");
			patientMap.put("room", patient.getBed().getRoom().getTitle());
			patientMap.put("bed", patient.getBed().getId().toString());
		}
		return  ok(gson.toJson(patientMap));
}

	public Result addPatient() throws Exception{

		String patient_id = formFactory.form().bindFromRequest().get("patient_id");
		String patient_fname = formFactory.form().bindFromRequest().get("first_name");
		String patient_lname = formFactory.form().bindFromRequest().get("last_name");
		String patient_diagnostic = formFactory.form().bindFromRequest().get("diagnostic");
		String admission_date = formFactory.form().bindFromRequest().get("admission_date");
		Integer patient_age = Integer.valueOf(formFactory.form().bindFromRequest().get("age"));
		Long bed_id = Long.valueOf(formFactory.form().bindFromRequest().get("bed_id"));
		Bed bed = Bed.find.byId(bed_id);

		Patient patient = new Patient();
		patient.setFname(patient_fname);
		patient.setLname(patient_lname);
		patient.setDiagnostic(patient_diagnostic);
		patient.setAge(patient_age);
		patient.setBed(bed);
		patient.setActive_flag(true);

		patient.save();

		return  redirect(controllers.routes.Admin.patients());

		// try {
		// 	DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		// 	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// 	Date result =  inputFormat.parse(admission_date.toString());
		// 	String outputTextDate = dateFormat.format(result);
		// } catch (ParseException pe) {
		// 	pe.printStackTrace();
		// }

}

	public Result addPatientInBed(){

//		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//
//		Date date = inputFormat.parse(patients.toString());
//		String outputTextDate = dateFormat.format(date);

		String patient_fname = formFactory.form().bindFromRequest().get("first_name");
		String patient_lname = formFactory.form().bindFromRequest().get("last_name");
		String patient_diagnostic = formFactory.form().bindFromRequest().get("diagnostic");
		Integer patient_age = Integer.valueOf(formFactory.form().bindFromRequest().get("age"));
		Long bed_id = Long.valueOf(formFactory.form().bindFromRequest().get("bed_id"));
		Bed bed = Bed.find.byId(bed_id);

		Patient patient = new Patient();
		patient.setFname(patient_fname);
		patient.setLname(patient_lname);
		patient.setDiagnostic(patient_diagnostic);
		patient.setAge(patient_age);
		patient.setBed(bed);
		patient.setActive_flag(true);

		patient.save();

		return  redirect(controllers.routes.Admin.departments());
	}

	public Result removePatient(Long patient_id) {
		Patient patient	 = Patient.find.byId(patient_id);
		this.lookupAdminStatus();
		if(this.isAdmin){
			return ok(this.gson.toJson(Patient.removePatient(patient_id)));
		}
		else{
			return ok(views.html.helper.errorPage.render(403, "Not allowed"));
		}
	}

//End Patient


// Check admin status
	private void lookupAdminStatus(){
		final User localUser = this.userProvider.getUser(session());
		if(localUser == null){
			this.isAdmin = false;
		}
		else{
			String user_id = localUser.getIdentifier();
			this.isAdmin = models.Admin.isUserAdmin(user_id);
		}
	}

	public static boolean loggedIn() {
		return (session().get("pa.u.id") != null);
	}


//	@Restrict(@Group(Admin.USER_ROLE))
//	public Result restricted() {
//		final User localUser = this.userProvider.getUser(session());
//		return ok(restricted.render(this.userProvider, localUser));
//	}
//
//	@Restrict(@Group(Admin.USER_ROLE))
//	public Result profile() {
//		final User localUser = userProvider.getUser(session());
//		return ok(profile.render(this.auth, this.userProvider, localUser));
//	}
}

package controllers;

import models.Person;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import service.UserProvider;
import views.html.person.*;

import javax.inject.Inject;
import java.util.List;

import static play.libs.Json.toJson;


/**
 * Created by Marius Sandu on 8/15/2016.
 */
public class Persons extends Controller {

    private final UserProvider userProvider;
    private final FormFactory formFactory;

    @Inject
    public Persons(final UserProvider userProvider, final FormFactory formFactory){
        this.userProvider = userProvider;
        this.formFactory = formFactory;
    }

    public Result index() {
        List<models.Person> persons = Person.getPersons();
        return ok(person.render(persons, this.userProvider));
    }

    public Result addPerson() {
        models.Person person = this.formFactory.form(models.Person.class).bindFromRequest().get();
        person.save();
        return redirect(routes.Application.index());
    }

    public Result removePerson(Long person_id){
        return ok(toJson(Person.removePerson(person_id)));
    }

}

package models;

import com.avaje.ebean.Model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import play.Logger;

import javax.persistence.*;
import java.util.List;

@Entity
public class Admin extends Model {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JoinColumn(name="user_id", referencedColumnName="id")
    private Integer user_id;

    private Boolean isSuperAdmin;

    public static Finder<Long, Admin> find = new Finder<Long,Admin>(Admin.class);

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Boolean getSuperAdmin() {
        return isSuperAdmin;
    }

    public void setSuperAdmin(Boolean superAdmin) {
        isSuperAdmin = superAdmin;
    }

    public static Boolean isUserAdmin(String user_id){
        Admin isAdmin = Admin.find.where().eq("user_id", user_id).findUnique();
        return isAdmin != null;
    }
}
package models;

import com.avaje.ebean.Model;
import play.Logger;

import javax.persistence.*;
import java.util.List;

@Entity
public class Bed extends Model {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne()
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    private Room room;

    private String username;

    private String password;

    private Boolean active_flag;

    private String bind_token;

    public static Finder<Long, Bed> find = new Finder<Long, Bed>(Bed.class);

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(Boolean active_flag) {
        this.active_flag = active_flag;
    }

    public String getBind_token() {
        return bind_token;
    }

    public void setBind_token(String bind_token) {
        this.bind_token = bind_token;
    }

    public Patient getPatient(){
        return Patient.find.where().eq("bed_id", this.getId()).findUnique();
    }

    public static Bed getBed(Long bed_id) {
        try {
            return Bed.find.byId(bed_id);
        } catch (Exception e) {
            Logger.info("could not find Beds with id " + bed_id);
            throw e;
        }
    }

    public static List<Bed> getAllBeds() {
        return (List<Bed>) Bed.find.where().eq("active_flag", true).findList();
    }

    public static List<Bed> getAllBedsByRoomId(Long room_id){
       return Bed.find.where().eq("room_id", room_id).eq("active_flag", true).findList();
    }

    public static Integer removeBed(Long bed_id) {
        Bed bedToRemove = Bed.find.byId(bed_id);
        if (bedToRemove != null) {
            try {
                bedToRemove.setActive_flag(false);
                bedToRemove.save();
                return 1;
            } catch (Exception e) {
                Logger.info("failed to remove Beds #" + bedToRemove.getId());
                throw e;
            }
        } else {
            return 0;

        }
    }

    public static Bed canLoginAsBed(Bed bed) throws Exception{
        try {
            return Bed.find.where().eq("username", bed.getUsername()).eq("password", bed.getPassword()).findUnique();
        } catch (Exception e) {
            throw e;
        }

    }

    public static Bed getBedByToken(String bindToken) throws Exception{
        try {
            return Bed.find.where().eq("bind_token", bindToken).findUnique();
        } catch (Exception e) {
            throw e;
        }

    }

}

package models;

import com.avaje.ebean.Model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import play.Logger;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
public class Department extends Model {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @OneToMany(mappedBy = "department_id", cascade = {CascadeType.REMOVE})
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Integer id;

    private String name;

    @OneToMany(cascade = CascadeType.REMOVE)
    private List<Room> rooms = new ArrayList<Room>();

    private Boolean active_flag;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(Boolean active_flag) {
        this.active_flag = active_flag;
    }

    public static Finder<Long, Department> find = new Finder<Long,Department>(Department.class);

    public static List<Department> getAllDepartments() {
        List<Department> departments = Department.find.where().eq("active_flag", true).findList();;
        return departments;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public static Integer removeDepartment(Long department_id){
        Department departmentToRemove = Department.find.byId(department_id);
        if(departmentToRemove != null){
            try{
                departmentToRemove.delete();
            }catch(Exception e){
                Logger.info("failed to remove Department #" + departmentToRemove.getId());
            }
            return 1;
        }
        else{
            return 0;

        }
    }

}
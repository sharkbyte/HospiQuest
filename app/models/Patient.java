package models;

import com.avaje.ebean.Model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import play.Logger;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Patient extends Model{


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @OnDelete(action = OnDeleteAction.CASCADE)

    private Integer id;

    private String fname;
    private String lname;
    @OneToOne()
    @JoinColumn(name="bed_id", referencedColumnName="id")
    private Bed bed;

    private String diagnostic;
    private Integer age;
    private Date admission_date;
    private Date extern_date;
    private Boolean active_flag;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }
    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getAdmission_date() {
        return admission_date;
    }
    public void setAdmission_date(Date admission_date) {
        this.admission_date = admission_date;
    }

    public Date getExtern_date() {
        return extern_date;
    }

    public void setExtern_date(Date extern_date ) {
        this.extern_date = extern_date;
    }

    public Bed getBed() {
        return bed;
    }

    public void setBed(Bed bed ) {
        this.bed = bed;
    }

    public Boolean getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(Boolean active_flag) {
        this.active_flag = active_flag;
    }

    public static Finder<Long, Patient> find = new Finder<Long,Patient>(Patient.class);

    public static List<Patient> getAllPatients() {
        List<Patient> patients = Patient.find.where().eq("active_flag", true).findList();
        return patients;
    }
    public static Patient getPatientById(Long patient_id){
        return Patient.find.byId(patient_id);
    }

    public static Patient getPatientByBedId(Long bed_id){
        return Patient.find.where().eq("bed_id", bed_id).findUnique();
    }

    public static Integer removePatient(Long patient_id) {
        Patient patientToRemove = Patient.find.byId(patient_id);
        if (patientToRemove != null) {
            try {
                patientToRemove.setBed(null);
                patientToRemove.setActive_flag(false);
                patientToRemove.save();
                return 1;
            } catch (Exception e) {
                Logger.info("failed to remove Patient #" + patientToRemove.getId());
                throw e;
            }
        } else {
            return 0;

        }
    }

}
package models;

import com.avaje.ebean.Model;
import play.Logger;

import javax.persistence.*;
import java.util.List;

@Entity
public class Person extends Model{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
	private Integer id;

    private String name;

    public static Finder<Long, Person> find = new Finder<Long,Person>(Person.class);

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Person> getPersons() {
        return (List<Person>) Person.find.all();
    }

    public static Integer removePerson(Long person_id){
        Person personToRemove = Person.find.byId(person_id);
        if(personToRemove != null){
            try{
                personToRemove.delete();
            }catch(Exception e){
                Logger.info("failed to remove person #" + personToRemove.getId());
            }
            return 1;
        }
        else{
            return 0;

        }
    }

}

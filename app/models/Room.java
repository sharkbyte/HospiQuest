package models;

import com.avaje.ebean.Model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import play.Logger;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Room extends Model{


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @OneToMany(mappedBy = "room_id", cascade = {CascadeType.REMOVE})
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Integer id;

    private String title;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Bed> beds = new ArrayList<Bed>();

    @ManyToOne()
    @JoinColumn(name = "department_id", referencedColumnName = "id")
    private Department department;

    private Boolean active_flag;

    public static Finder<Long, Room> find = new Finder<Long,Room>(Room.class);

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title; }

    public List<Bed> getBeds() {
        return beds;
    }

    public void setBeds(List<Bed> beds) {
        this.beds = beds;
    }

    public Boolean getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(Boolean active_flag) {
        this.active_flag = active_flag;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public static Integer removeRoom(Long room_id){
        Room roomToRemove = Room.find.byId(room_id);
        if(roomToRemove != null){
            try{
                roomToRemove.delete();
                Logger.info("removing Room #" + roomToRemove.getId());
            }catch(Exception e){
                Logger.info("failed to remove Room #" + roomToRemove.getId());
            }
            return 1;
        }
        else{
            return 0;

        }
    }

    public static List<Room> getAllRoomsByDepartmentId(Long department_id){
        return Room.find.where().eq("department_id", department_id).eq("active_flag", true).findList();
    }


}
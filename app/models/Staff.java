package models;

import com.avaje.ebean.Model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import play.Logger;

import javax.persistence.*;
import java.util.List;

@Entity
public class Staff extends Model {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String fname;
    private String lname;
    private Department department;
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name="role_status", referencedColumnName="id")
    private Role role_status;


    public Integer getId() { return id; }
    public void setId(Integer id) { this.id = id; }

    public String getFname() { return  fname; }
    public void setFname(String fname) { this.fname = fname; }

    public String getLname() { return  lname; }
    public void setLname(String lname) { this.lname = lname; }

    public Department department() { return department ; }
    public void  setDepartment(Department department) { this.department = department; }


    public Role getRole_status() {
        return role_status;
    }

    public void setRole_status(Role role_status) {
        this.role_status = role_status;
    }
}